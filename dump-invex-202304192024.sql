-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: invex
-- ------------------------------------------------------
-- Server version	11.1.0-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `Id_invex` int(11) NOT NULL,
  `Id_Binance` int(11) DEFAULT NULL,
  `Username` varchar(100) NOT NULL,
  `Password` varchar(1000) NOT NULL,
  `Cedula` varchar(20) NOT NULL,
  `Nacionalidad` int(11) NOT NULL,
  `Cuidad_Residencia` int(11) NOT NULL,
  `Codigo_postal` int(11) NOT NULL,
  `Verificado` tinyint(1) NOT NULL,
  `Fecha_cumple` date NOT NULL,
  `Empresa` varchar(100) NOT NULL,
  `Cargo` varchar(100) NOT NULL,
  `exposedPolitically` tinyint(1) NOT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `First_name` varchar(100) DEFAULT NULL,
  `Last_name` varchar(100) DEFAULT NULL,
  `Address` varchar(500) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id_invex`),
  UNIQUE KEY `Id_Binance` (`Id_Binance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'djgar','pbkdf2:sha256:260000$pY07ZBRvp7yEyoIp$1cb50a8e8f6608d69b3cf938f99f7049aebf02a172fac913fadf570f248598e7','001-22222222',25,32,11111,0,'2023-10-02','free','desarrollador/backend',0,'djgar98@gmail.com','Danny Jossué','Garcia Gonzalez','Villa Venezuela, de la Iglesia madre de Dios, 1 cuadra oeste,',1),(2,2,'froy','pbkdf2:sha256:260000$pY07ZBRvp7yEyoIp$1cb50a8e8f6608d69b3cf938f99f7049aebf02a172fac913fadf570f248598e7','01-333333',2,2,20000,0,'2000-10-02','Inxex','Admin/Inversionista',0,'froy@gmail.com','Froylan','Rodriguez','Quito',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'invex'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-04-19 20:24:10
