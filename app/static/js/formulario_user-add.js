const formulario_user = document.getElementById('formulario_user');
const inputs_pass = document.querySelectorAll('#formulario_user input');

const expresiones = {
	username: /^[a-zA-Z0-9\_\-]{4,16}$/, // Letras, numeros, guion y guion_bajo
	name: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	address: /^[a-z0-9A-Z09À-ÿ,./|!@#$%^&*())-_\s0/]{1,300}$/, // Letras y espacios, pueden llevar acentos.
	password: /^.{4,12}$/, // 4 a 12 digitos.
	email: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
	telefono: /^\d{7,14}$/, // 7 a 14 numeros.
	numero: /^\d{1,14}$/, // 7 a 14 numeros.
	cedula: /^\d{10}$/, // 10 numeros.
	binance: /^\d{9,10}$/, // 7 a 14 numeros.
	postal: /^\d{5}(?:[-s]d{4})?$/,
	nombre_cargo: /^[a-zA-ZÀ-ÿ\s/]{1,40}$/,
}


const campos = {
	name: false,
	last: false,
	address: false,
	cuidad: false,
	pais: false,
	postal: false,
	empresa: false,
	cargo: false
}


const validarCampo = (expresion, input, campo) => {
	if(expresion.test(input.value)){
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-check-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-times-circle');
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.remove('formulario__input-error-activo');
		campos[campo] = true;
	} else {
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-correcto');
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-times-circle');
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-check-circle');
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.add('formulario__input-error-activo');
		campos[campo] = false;
	}
}

const validarFormulario = (e) => {
	switch (e.target.name) {
		case "username":
			validarCampo(expresiones.username, e.target, 'username');
			//console.log('Presiono el text name')
		break;
		case "email":
			validarCampo(expresiones.email, e.target, 'email');
			//validarPassword2();
			console.log('Presiono el text email')
		break;
		case "name":
			//validarPassword2();
			//console.log('Presiono el text name')
			validarCampo(expresiones.name, e.target, 'name');
		break;
		case "last":
			//validarPassword2();
			//console.log('Presiono el text apellido')
			validarCampo(expresiones.name, e.target, 'last');
		break;
		case "cedula":
			//validarPassword2();
			validarCampo(expresiones.cedula, e.target, 'cedula');
		break;
		case "binance":
			//validarPassword2();
			validarCampo(expresiones.binance, e.target, 'binance');
		break;
		case "address":
			//validarPassword2();
			//console.log('Presiono el text direccion')
			validarCampo(expresiones.address, e.target, 'address');
		break;
		case "cuidad":
			//validarPassword2();
			//console.log('Presiono el text cuidad')
			validarCampo(expresiones.numero, e.target, 'cuidad');
		break;
		case "pais":
			//validarPassword2();
			//console.log('Presiono el text pais')
			validarCampo(expresiones.numero, e.target, 'pais');
		break;
		case "postal":
			//validarPassword2();
			//console.log('Presiono el text postal')
			validarCampo(expresiones.postal, e.target, 'postal');
		break;
		case "empresa":
			//validarPassword2();
			//console.log('Presiono el text empresa')
			validarCampo(expresiones.name, e.target, 'empresa');
		break;
		case "cargo":
			//validarPassword2();
			//console.log('Presiono el text cargo')
			validarCampo(expresiones.nombre_cargo, e.target, 'cargo');
		break;
	}
}

inputs_pass.forEach((input) => {
	input.addEventListener('keyup', validarFormulario);
	input.addEventListener('blur', validarFormulario);
});



formulario_user.addEventListener('submit', (e) => {
	const terminos = document.getElementById('terminos');
	if(campos.name  && campos.last && campos.address && campos.cuidad && campos.pais && campos.postal && campos.empresa && campos.cargo){
		document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
		document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');
		setTimeout(() => {
			document.getElementById('formulario__mensaje-exito').classList.remove('formulario__mensaje-exito-activo');
		}, 5000);

		document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
			icono.classList.remove('formulario__grupo-correcto');
		});
	} else {
		document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
		e.preventDefault();
	}
});