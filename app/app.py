from flask import Flask, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL
from flask_wtf.csrf import CSRFProtect
from flask_login import LoginManager, login_user, logout_user, login_required, current_user
from functools import wraps


from config import config

# Models:
from models.ModelUser import ModelUser

# Entities:
from models.entities.user import User

app=Flask(__name__)

csrf = CSRFProtect()
db = MySQL(app)
login_manager_app = LoginManager()
login_manager_app.init_app(app)

@login_manager_app.user_loader
def user_loader(user_id):
    return ModelUser.get_by_id(db, user_id)


def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kws):
        is_admin = getattr(current_user, 'is_admin', False)
        if not is_admin:
            return render_template('not_admin.html')
        return f(*args, **kws)
    return decorated_function


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        user = User(1, request.form['username'], request.form['password'],1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)
        logged_user = ModelUser.login(db, user)
        if logged_user != None:
            if logged_user.password:
                login_user(logged_user)
                return redirect(url_for('home'))
            else:
                flash("Invalid password...")
                return render_template('login.html')
        else:
            flash("User not found...")
            return render_template('login.html')
    else:
        return render_template('login.html')
    
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

def status_401(error):
    return redirect(url_for('login'))


@app.route('/home')
@login_required
def home():
    return render_template('home.html')


@app.route('/user')
@login_required
def profileUser():
    user = ModelUser.get_items_users(db, current_user.id )
    return render_template('user.html', user = user)



@app.route('/admin-home')
@login_required
@admin_required
def profileAdmin():
    return render_template('base-admin.html')


@app.route('/add-user', methods=['GET', 'POST'])
@login_required
@admin_required
def addUser():
    return render_template('add-user.html')



@app.route('/password', methods=['GET', 'POST'])
@login_required
def updatepassword():
    if request.method == 'POST':
        password = request.form['password']
        password_1 = request.form['password_1']
        if not password.strip() or not password_1.strip():
            flash("Debe completar todos los campos")
        else:
            if password == password_1:
               user = User(1, 1, request.form['password'],1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)
               pass_update = ModelUser.update_password_user(db, user, current_user.id)
               if pass_update == 1:
                   return render_template('pass.html')
            else:
                flash("Las contraseñas son diferentes")
                return render_template('pass.html')
    else:
        # user = ModelUser.get_items_users(db, current_user.id )
        return render_template('pass.html')



@app.route('/user_update' , methods=['GET', 'POST'])
@login_required
def updateUser():
    if request.method == 'POST':
        user = User(1, 1,1, 1, 1, request.form['pais'], request.form['cuidad'], request.form['postal'], 1, 1, request.form['empresa'], request.form['cargo'], 1, 1, request.form['name'], request.form['last'], request.form['address'],1)
        update_user = ModelUser.update_items_users(db, user, current_user.id)
        if update_user == 1:
            user = ModelUser.get_items_users(db, current_user.id )
            return render_template('user.html', user = user)
    else:
        return render_template('user.html')


def status_404(error):
    return render_template('404.html'), 404


if __name__ =='__main__':
    app.config.from_object(config['development'])
    csrf.init_app(app)
    app.register_error_handler(401, status_401)
    app.register_error_handler(404, status_404)
    app.run()
