from .entities.user import User
from werkzeug.security import generate_password_hash


class ModelUser():

    @classmethod
    def login(self, db, user):
        try:
            cursor = db.connection.cursor()
            sql = """SELECT Id_invex, Username, Password FROM Users 
                    WHERE username = '{}'""".format(user.username)
            cursor.execute(sql)
            row = cursor.fetchone()
            if row != None:
                user = User(row[0], row[1], User.check_password(row[2], user.password), None, None, None, None, None, None, None, None, None, None, None, None, None, None, None)
                return user
            else:
                return None
        except Exception as ex:
            raise Exception(ex)

    @classmethod
    def get_by_id(self, db, id):
        try:
            cursor = db.connection.cursor()
            sql = "SELECT Id_invex, Username, is_admin FROM users WHERE Id_invex = {}".format(id)
            cursor.execute(sql)
            row = cursor.fetchone()
            if row != None:
                return User(row[0], row[1], None,  None, None, None, None, None, None, None, None, None, None, None, None, None, None, row[2])
            else:
                return None
        except Exception as ex:
            raise Exception(ex)
        

    @classmethod
    def get_items_users(self, db, id):
        try:
            cursor = db.connection.cursor()
            print('es el valor de id')
            print(id)
            sql = """SELECT Username, Id_Binance, Cedula, Nacionalidad, Cuidad_Residencia, Codigo_postal, Fecha_cumple, 
            Empresa, Cargo, Email, First_name, Last_name, Address FROM Users 
            WHERE Id_invex = {}""".format(id)
            cursor.execute(sql)
            row = cursor.fetchone()
            if row != None:
                return User(None, row[0], None, row[1], row[2], row[3], row[4], row[5], None, row[6], row[7], row[8], None, row[9], row[10], row[11], row[12], None)
            else:
                return None
        except Exception as ex:
            raise Exception(ex)
        

    @classmethod
    def update_items_users(self, db, user, id):
        try:
            cursor = db.connection.cursor()
           # print('es el valor de id')
           # print(id)
            sql = """UPDATE Users set Nacionalidad = %s, Cuidad_Residencia = %s, 
            Codigo_postal = %s, Empresa = %s, Cargo = %s, First_name = %s, 
            Last_name = %s, Address = %s 
            WHERE Id_invex = %s"""
            data  = (user.nacionalidad, user.residencia, user.postal, user.empresa, user.cargo, user.name, user.last, user.address, id)
            cursor.execute(sql, data)
            db.connection.commit()
            return 1
        except Exception as ex:
            raise Exception(ex)
        

    @classmethod
    def update_password_user(self, db, user, id):
        try:
            cursor = db.connection.cursor()
            #print('es el valor de id')
            #print(id)
            password = generate_password_hash(user.password)
            #print(password)
            sql = """UPDATE Users set Password = %s
            WHERE Id_invex = %s"""
            data  = (password, id)
            cursor.execute(sql, data)
            db.connection.commit()
            return 1
        except Exception as ex:
            raise Exception(ex)