from werkzeug.security import check_password_hash
from flask_login import UserMixin


class User(UserMixin):

    def __init__(self, Id_invex, Username, Password, Id_Binance, Cedula, Nacionalidad, Cuidad_Residencia, Cod_postal, Verificado, Fecha_cumple, Empresa, Cargo, Exposed, Email, Frist_name, Last_name, address, is_admin) -> None:
        self.id = Id_invex
        self.username = Username
        self.password = Password
        self.id_binance = Id_Binance
        self.cedula = Cedula
        self.nacionalidad = Nacionalidad
        self.residencia = Cuidad_Residencia
        self.postal = Cod_postal
        self.verificado = Verificado
        self.cumple = Fecha_cumple
        self.empresa = Empresa
        self.cargo = Cargo
        self.exposed = Exposed
        self.email = Email
        self.name= Frist_name
        self.last = Last_name
        self.address = address
        self.is_admin = is_admin

    @classmethod
    def check_password(self, hashed_password, Password):
        return check_password_hash(hashed_password, Password)
 